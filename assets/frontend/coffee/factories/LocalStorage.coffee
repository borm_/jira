module.exports = ($http, $q, localStorageService)->

    defer = $q.defer()

    return {

        Users : ()->
            Users = localStorageService.get('Users')
            if Users isnt null then defer.resolve(Users)
            else $http.get('/api/users').then (response)->
                localStorageService.set('Users',response.data.users)
                defer.resolve(response.data.users)
            defer.promise

        Types : ()->
            Types = localStorageService.get('Types')
            if Types isnt null then defer.resolve(Types)
            else $http.get('/api/types').then (response)->
                localStorageService.set('Users',response.data.types)
                defer.resolve(response.data.types)
            defer.promise

        all : ()->
            # http://fdietz.github.io/recipes-with-angular-js/consuming-external-services/deferred-and-promise.html
            # https://github.com/fdietz/recipes-with-angular-js-examples/blob/master/chapter5/recipe4/app/js/controllers.js
            #Users = localStorageService.get('Users')
            Users = null
            if Users is null
                Users = $http.get('/api/users').then (response)->
                    localStorageService.set('Users',response.data.users)
                    response.data.users

            Types = localStorageService.get('Types')
            if Types is null
                Types = $http.get('/api/types').then (response)->
                    localStorageService.set('Types',response.data.types)
                    response.data.types

            Groups = localStorageService.get('Groups')
            if Groups is null
                Groups = $http.get('/api/groups').then (response)->
                    localStorageService.set('Groups',response.data.groups)
                    response.data.groups

            Statuses = localStorageService.get('Statuses')
            if Statuses is null
                Statuses = $http.get('/api/statuses').then (response)->
                    localStorageService.set('Statuses',response.data.statuses)
                    response.data.statuses

            Priorities = localStorageService.get('Priorities')
            if Priorities is null
                Priorities = $http.get('/api/priorities').then (response)->
                    localStorageService.set('Priorities',response.data.priorities)
                    response.data.priorities


            $q.all(
                users       : Users
                types       : Types
                groups      : Groups
                statuses    : Statuses
                priorities  : Priorities
            ).then (result) ->
                defer.resolve(result)
                return

            defer.promise

    }
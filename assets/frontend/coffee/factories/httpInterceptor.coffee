module.exports = (Alertify, $q, $log)->

    response: (response) ->
        #$log.debug response
        # Alertify.notify "success with status #{response.status}"
        response || $q.when response

    responseError: (rejection) ->
        console.log rejection
        #$log.debug "error with status #{rejection.status} and data: #{rejection.data['message']}"
        switch rejection.status
            when 404
                Alertify.set('notifier','position', 'top-right');
                Alertify.error rejection.statusText
            when 500
                Alertify.set('notifier','position', 'top-right');
                Alertify.error rejection.statusText
            when 0
                $log.debug "No connection, internet is down?"
            else
                $log.debug "#{rejection.data['message']}"

        # do something on error
        $q.reject rejection
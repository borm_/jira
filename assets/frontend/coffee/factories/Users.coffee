module.exports = ($http,$q)->
    defer = $q.defer()

    $http.get '/api/users'
    .success (data)->
        defer.resolve data.users

    defer.promise
module.exports = (
        $scope
        $http
        localStorageService
        $localStorage
        $modal
        $log
        $rootScope
        FileUploader
    )->

    $scope.static   = '/static'
    $scope.media   = '/media'

    $localStorage.all().then (obj)->

        $scope.tasks =
            groups  : {}
            list    : {}

        $scope.users = _.reduce obj.users, (o,v)->
            o[v.id] = v
            o
        , {
            0 : {
                id: 0
                email: null
                name: "Unasigned"
                avatar: "noavatar.png"
                role: "none"
            }
        }

        $scope.types = _.reduce obj.types, (o,v)->
            o[v.id] = v
            o
        , {}

        $scope.groups = _.reduce obj.groups, (o,v)->
            o[v.id] = v
            $scope.tasks.groups[v.class] = []
            o
        , {}

        $scope.statuses = _.reduce obj.statuses, (o,v)->
            o[v.id] = v
            o
        , {}

        $scope.priorities = _.reduce obj.priorities, (o,v)->
            o[v.id] = v
            o
        , {}

        $http.get('/api/tasks')
        .success (data)->
            console.log data
            _.each data.tasks, (task)->
                task.assign     = _.assign {},$scope.users[task.assign]
                task.author     = _.assign {},$scope.users[task.author]
                task.type       = _.assign {},$scope.types[task.type]
                task.group      = _.assign {},$scope.groups[task.group]
                task.status     = _.assign {},$scope.statuses[task.status]
                task.priority   = _.assign {},$scope.priorities[task.priority]
                $scope.tasks.groups[task.group.class].push task
                return
                #$scope.tasks.list[task.id] = task
            #$scope.tasks.list = data.tasks
            delete data.tasks

        $scope.preview = false

        $scope.newTask =
            data    :
                author      : 0
                assign      : 0
                title       : 'New Task'
                message     : 'Create New Task'
                description : 'Create New Task'
                type        : 4
                priority    : 3
                group       : 1
                status      : 0
                time        : 0
                attachments : []
            modal   : ()->
                modalInstance = $modal.open({
                    templateUrl: 'myModalContent.html'
                    size: 'lg'
                    backdrop : false
                    keyboard  : false
                    resolve: {
                        task: ()->
                            _.assign({},$scope.newTask.data)
                    }
                    scope : do ->
                        scope = $rootScope.$new()
                        #scope.task = _.assign({},$scope.newTask.data)

                        scope.types = $scope.types
                        scope.priorities = $scope.priorities
                        scope.users = $scope.users
                        scope
                    controller: ($scope, $modalInstance, task)->
                        $scope.task = task
                        $scope.Files = []
                        $uploader = $scope.uploader = new FileUploader
                            url : '/api/upload/image/'
                            add : (previews)->
                                $scope.Previews = previews
                                $scope.Names = $scope.Previews.map (item)->
                                    item.name
                                console.log $scope.Names
                                return

                        $scope.ok = ()->
                            $http.post '/Task/create', $scope.task
                            .success (data) ->

                                $scope.task.id = data.id
                                if $scope.Previews
                                    $uploader.uploadFiles().then (files)->
                                        $scope.task.attachments = files
                                        $http.post '/Task/attach/Add', $scope.task
                                        .then ()->
                                            $modalInstance.close $scope.task
                                else
                                    $modalInstance.close $scope.task

                        $scope.cancel = ()->
                            $modalInstance.dismiss('cancel')
                });
                modalInstance.result.then (task)->
                    console.log task
                    task.assign     = _.assign {},$scope.users[task.assign]
                    task.author     = _.assign {},$scope.users[task.author]
                    task.type       = _.assign {},$scope.types[task.type]
                    task.group      = _.assign {},$scope.groups[task.group]
                    task.status     = _.assign {},$scope.statuses[task.status]
                    task.priority   = _.assign {},$scope.priorities[task.priority]
                    task.time       = Date.now() / 1000
                    $scope.tasks.groups['todo'].push(task)
                    #$scope.tasks.list.push(task)
                    #$scope.selected = task;

                , ()->
                    $log.info('Modal dismissed at: ' + new Date());

        return false
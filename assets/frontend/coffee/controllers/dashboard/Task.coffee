module.exports =
    url         : "/task/:id"
    templateUrl : "/modules/dashboard/Task.hbs"
    controller  : ($scope,$http,getTask,preview)->
        task = getTask.data.task

        task.assign     = _.assign {},$scope.users[task.assign]
        task.author     = _.assign {},$scope.users[task.author]
        task.type       = _.assign {},$scope.types[task.type]
        task.group      = _.assign {},$scope.groups[task.group]
        task.status     = _.assign {},$scope.statuses[task.status]
        task.priority   = _.assign {},$scope.priorities[task.priority]
        $scope.task = task

        $scope.preview = preview

        ## Change Task Assign
        $scope.$watchCollection 'task.assign',(assign,ov)->
            if assign isnt ov
                $http.post('/Task/change/Assign',{
                    aid : assign.id
                    tid : task.id
                }).success (data) ->
                    findTaskInGroup task.group.class,task.id ,(task)->
                        task.assign = _.assign task.assign,assign
                    console.log data

        ## Change Task Group
        $scope.changeGroup = (tid,gid)->
            $http.post('/Task/change/Group',{
                tid : tid
                gid : gid
            }).success (data) ->
                $scope.tasks.groups[task.group.class] = _.filter(
                    $scope.tasks.groups[task.group.class],(item)->
                        item.id isnt tid
                )
                $scope.tasks.groups[$scope.groups[gid].class].push $scope.task
                $scope.task.group = _.assign $scope.task.group,$scope.groups[gid]
                console.log $scope.tasks.groups
                return
            return

        ###
            @params task.id
            @params group.key
        ###
        findTaskInGroup = (group,id,cb)->
            _.each $scope.tasks.groups[group], (item,index)->
                if item.id is id
                    cb.call(null,item,index)
                    return false
            return

        ###
            Comments
        ###
        $scope.comment =
            text: ''
            Add : (tid)->
                if $scope.comment.text isnt ''
                    $http.post('/Task/comment/Add', {
                        tid : tid
                        text: $scope.comment.text
                    }).success (data)->
                        console.log data
                        $scope.comment.text = ''
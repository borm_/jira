module.exports =

    'dashboard' :
        url         : "/RapidBoard"
        templateUrl : "/modules/dashboard/List.hbs"
        controller  : ($scope, $http, $stateParams, $state, $timeout)->
            $scope.preview = false
            $scope.sortableOptions =
                handle      : '> .task-handle'
                placeholder : "task-placeholder"
                connectWith : ".tasks-col"
                start: (e, ui)->
                    ui.placeholder.height(ui.item.outerHeight())
                stop : (e,ui)->
                    console.log e
                    console.log ui
                    console.log ui.item.sortable.model
                    if ui.item.sortable.droptarget
                        id = ui.item.sortable.droptarget.data().id
                        ui.item.sortable.model.group = _.assign ui.item.sortable.model.group,$scope.groups[id]
                        changeGroup(ui.item.sortable.model)
                    console.log $scope.tasks.groups

            changeGroup = (model)->
                $http.post('/Task/change/Group',{
                    tid : model.id
                    gid : model.group.id
                }).success (data) ->
                    console.log data
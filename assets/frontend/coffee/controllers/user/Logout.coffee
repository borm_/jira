module.exports =
    #abstract    : true
    url         : "/logout"
    controller  : ($scope, $http, $state)->
        $http.post '/logout',{}
        .success (data)->
            console.log data
            location.href = '/RapidBoard' if data.status is 'success'

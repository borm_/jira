module.exports =
    url         : "/signup"
    templateUrl : "/modules/user/SignUp.hbs"
    resolve     :
        messages : [
            '$http', ($http)->
                $http({method: 'POST',url: '/signup'})
        ]
    controller  : ($scope, $http,Alertify)->

        $scope.user =
            email : ''
            passwd: ''
            role  : 1

        $scope.signUp = ()->
            $http.post '/signup' , $scope.user
            .success (data, status, headers, config)->
                console.log data
                if data.status is 'warning'
                    Alertify.set 'notifier','position','top-right'
                    Alertify.warning data.message
                return false;
            .error (data, status, headers, config)->
                console.log data
                return false;
            return false;

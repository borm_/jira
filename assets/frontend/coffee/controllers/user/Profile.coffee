module.exports =
    url         : "/profile"
    templateUrl : "/modules/user/Profile.hbs"
    resolve     :
        response    : ($http)->
            $http({method: 'GET',url: '/profile/Get/'})
    controller  : ($scope, $http, FileUploader, Alertify, response)->
        $scope.user = response.data.user if response.data.status is 'success'
        $scope.Files = []
        $uploader = $scope.uploader = new FileUploader
            url : '/api/upload/avatar/'
            add : (preview)->
                $scope.Avatar = preview[0]
                $scope.Name = [preview[0].name]
                return

        $scope.Save = ()->
            $http.post '/profile/Save', $scope.user
            .success (data) ->
                if $scope.Avatar
                    $uploader.uploadFiles().then (files)->
                        $http.post '/profile/Avatar', {
                            avatar : files[0]
                        }
                        .success (data)->
                            $scope.user.avatar = data.avatar
                            Alertify.success('Success')
                            $scope.Avatar = []
                            $scope.Name = []
                            $uploader.clear()
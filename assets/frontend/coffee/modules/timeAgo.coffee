angular.module('time.ago', [])
.value(
    'TimeAgoConfig'
    lng : 'en'
)
.factory 'TimeAgo',[
        'TimeAgoConfig'
        '$timeout'
        (Config,$timeout)->

            class timeAgo
                constructor : (elem,time)->
                    @elem = elem
                    @time = time / 1000
                    @timeout = 0
                    @out = 0
                    @change()
                    return

                change      : ()->
                    @calculate()
                    out = @out
                    if out is 0 || !document.body.contains @elem[0]
                        @destroy()
                        return

                    @timeout = $timeout((->
                        @change()
                        return
                    ).bind(@), out)
                    return

                calculate   : ()->
                    month =
                        'ru' : [
                            'января','февраля',
                            'марта','апреля','мая',
                            'июня','июля','августа',
                            'сентября','октября','ноября',
                            'декабря'
                        ]
                        'en' : [
                            'Jan','Feb',
                            'Mar','Apr','May',
                            'Jun','Jul','Aug',
                            'Sep','Oct','Nov',
                            'Dec'
                        ]

                    times =
                        'ru' : [
                            ' сек. назад',' мин. назад',' час. назад'
                        ]
                        'en' : [
                            ' sec. ago',' min. ago',' h. ago'
                        ]

                    ts = Math.round Date.now() / 1000
                    now = new Date @time*1000
                    $dif = ts - @time

                    time = 0; out = 0

                    if $dif < 60
                        time = $dif + times[Config.lng][0]
                        out = 10
                    else if $dif >= 60 and $dif / 60 < 60
                        time = Math.round($dif / 60) + times[Config.lng][1]
                        out = 60
                    else if $dif / 3600 >= 1 and $dif / 3600 < 24
                        time = Math.round($dif / 3600) + times[Config.lng][2]
                        out = 3600
                    else
                        $month = month[Config.lng][now.getMonth()]
                        $day = now.getDate()
                        $year = now.getFullYear()
                        $hour = now.getHours()
                        $min = now.getMinutes()
                        time = $day + ' ' + $month + ' ' + $year + ' г. в ' + @format($hour) + ':' + @format($min)
                        out = 0
                    @elem.text time
                    @out = out * 1000
                    return

                format      : (i)->
                    i = '0' + 1 if (i<10)
                    return i


                destroy     : ()->
                    $timeout.cancel(@timeout);
                    @elem.removeData('time_ago_instance')
                    return

        ]
.directive 'timeAgo', [
    'TimeAgo'
    '$timeout'
    (TimeAgo)->
        restrict: 'EA'
        scope: {
            timeAgo: '='
        }
        link: (scope, element) ->

            if !element.data('time_ago_instance')
                element.data(
                    "time_ago_instance",
                    new TimeAgo(element,
                    new Date(scope.timeAgo).getTime())
                )

            scope.$on "$destroy", ()->
                element.data('time_ago_instance').destroy()
                return

            return
    ]
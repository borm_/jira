'use strict'

angular.module('Uploader', [])
.value(
    'upConfig', {
    # Options
    url:           ''              # Upload url
    fieldName:     'Files'         # Key in $_FILES array
    fileClass:     'dragover-file' # Draggable file class name
    notFileClass:  'dragover-plain'# Draggable non-file class name

    # Fields added to the model
    fileName:      'filename'
    fileThumb:     'thumb'
    fileSize:      'size'
    fileLoaded:    'loaded'
    fileProgress:  'progress'
    fileUploading: 'uploading'

    # Fields added to the scope
    queue: 'uploading' # Uploading queue. Contains a general options:
    #  queue.total    - all files size, bytes
    #  queue.loaded   - all files loaded, bytes
    #  queue.progress - all files upload percentage
    #  queue.all      - number of uploaded files
    #  queue.lenght   - number of remaining files (native option)
    validate: (file, permit) ->
        errors = []
        if typeof permit == 'object'
            if permit['allowedType'] == null or permit['allowedType'].indexOf(file[@fileName].toLowerCase().split('.').pop()) == -1
                errors.push
                    msg: permit['errorBadType']
                    code: 'type'
            if file[@fileSize] > (if +permit['maxSize'] then permit['maxSize'] else Infinity)
                errors.push
                    msg: permit['errorBigSize']
                    code: 'size'
            if file[@fileSize] > (if +permit['maxSpace'] then permit['maxSpace'] else Infinity) - (permit['space'])
                errors.push
                    msg: permit['errorMaxSize']
                    code: 'full'
            if permit['maxNumberOfFiles'] and permit['quantity'] >= permit['maxNumberOfFiles']
                errors.push
                    msg: permit['errorMaxQuantity']
                    code: 'quantity'
        if errors.length then errors else false
    setError: (code, data) ->
        response = data.data
        errors = if response then (if response.error then [ response.error ] else response) else []
        if !errors.length
            switch code
                when 'drop'
                    errors.push
                        msg: 'Drag and drop is not supported. Upgrade your browser'
                        code: code
                when 'validate'
                    errors.push
                        msg: 'The file is invalid'
                        code: code
                when 'preview'
                    errors.push
                        msg: 'Thumbnails are not supported. Upgrade your browser'
                        code: code
                when 'load'
                    errors.push
                        msg: 'Unable to load file. No connection to the Internet'
                        code: code
                when 'upload'
                    errors.push
                        msg: 'Unable to load file. Server problem'
                        code: code
                when 'abort'
                    errors.push
                        msg: 'Download aborted'
                        code: code
        if data.item[@fileLoaded]
            delete data.item[@fileLoaded]
            delete data.item[@fileProgress]
            delete data.item[@fileUploading]

        item: data.item
        response: errors

    change: (file)->
        file.$upload @url, {}
    done : (message)->
        console.log message
})
.directive('upFile', ['upConfig', '$q', '$timeout', (UpConfig, $q, $timeout)->

    # Очередь файлов для загрузки.
    # Вынесена отдельно, т.к.
    #   упрощает поочередную загрузку фалов и решает проблему циклических ссылок на xhr
    queue = [];
    queue.index = 0

    link: (scope, element, attrs)->
        scope.thumbs = []
        scope.files = []
        scope.names = []
        opts = {};
        angular.extend(opts, UpConfig);

        scope.$watch attrs['upFile'], (nv)->
            opts = angular.extend({}, UpConfig, nv)
            queue.index = 0
        , true

        scope[opts.queue] = queue

        element.on 'change', ()->
            _add if @files then @files else this
            return

        _add = (files) ->
            if typeof opts.change is 'function'

                if angular.isElement(files)
                    input = angular.element(files)
                    form = angular.element('<form style="display: none;" />')
                    iframe = angular.element('<iframe src="javascript:false;" name="iframeTransport' + +new Date + '">')
                    clone = $compile(input.clone())(scope)
                    value = input.val()
                    files = [{
                        lastModifiedDate: null
                        size: null
                        type: 'like/' + value.replace(/^.+\.(?!\.)|.*/, '')
                        name: value.match(/[^\\]+$/)[0]
                        _form: form
                    }]
                    input.after(clone).after form
                    form.append(input).append iframe

                scope.$apply (scope) ->
                    i = 0
                    n = files.length
                    scope.names = []
                    while i < n
                        #Create a file object to be combined with a model
                        #Создаем объект файла для объединения с моделью
                        file =
                            $preview    : _preview
                            $upload     : _upload
                            $abort      : _abort
                            _file: files[i]
                        file[opts.fileName] = files[i].name
                        file[opts.fileThumb] = files[i].thumb
                        file[opts.fileSize] = files[i].size
                        file[opts.fileLoaded] = 0
                        file[opts.fileProgress] = 0
                        file[opts.fileUploading] = true
                        #Pass each file into a function that uploads it
                        #Передаем каждый файл в функцию, которая отправит его на загрузку
                        scope.files.push(file)
                        scope.names.push(file[opts.fileName])
                        opts.change file
                        i++
                    # Fix for ngList
                    ###scope.names = []
                    angular.forEach scope.files, (file)->
                        scope.names.push(file.filename)###

                    return

            return

        _preview = (item, thumb) ->
            previewDeferred = $q.defer()
            data =
                item: item
                response: {}
            thumb = if thumb then thumb else opts.fileThumb

            if window.FileReader != null
                reader = new FileReader
                reader.onload = ((file) ->
                    return (event) ->
                        if event.target.result.indexOf('data:image') isnt -1
                            scope.$apply ->
                                #If the file is read
                                #Если файл прочитан
                                item[thumb] = reader.result
                                data.response[thumb] = reader.result
                                scope.thumbs.push(item)
                                previewDeferred.resolve data
                                return
                            return
                )(this)
                reader.readAsDataURL @_file
            else if typeof opts.preview is 'function'
                #Reading files is not supported
                #Чтение файлов не поддерживается
                previewDeferred.reject opts.setError(
                    'preview',
                    item: item
                    response: null
                )
            previewDeferred.promise

        #Upload files
        _upload = (url, item, permit) ->
            uploadDeferred = $q.defer()
            #Download the following file starts when the previous loaded or aborted
            #Загрузка следующего файла начинается когда предыдущий загружен либо отменен

            _uploadQueue = ->
                if queue.length
                    if queue[0].item._file._form
                        $timeout ->
                            _iframeTransport(queue[0])
                            return
                        ,500
                    else
                        $timeout ->
                            _xhrTransport(queue[0])
                            return
                        ,500
                    queue[0].deferred.promise.finally ->
                        #for AngularJS 1.2. Use all instead of finally in old versions
                        #Delete this and move on to the next element in the queue in case the download is complete or an error (including due to cancellation)
                        #Удаляем этот и переходим к следующему элементу очереди в случае завершения загрузки или ошибки (в т. ч. из-за отмены)
                        queue.shift()
                        _uploadQueue()
                        return
                else
                    #Clear the queue when all files have been uploaded
                    #Обнуляем очередь когда все файлы загружены
                    delete queue.total
                    delete queue.loaded
                    delete queue.all
                    delete queue.progress
                    element.val ''
                    scope.names = []
                    _done()
                return

            item = angular.extend({}, item, this)
            delete item.$upload
            delete item.$preview
            errors = opts.validate(item, permit)
            if errors
                uploadDeferred.reject opts.setError('validate',
                    item: item
                    data: errors)
                return uploadDeferred.promise
            #Collect all the necessary data to load into a single object and place it in the queue
            #Собираем все необходимые для загрузки данные в один объект и помещаем его в очередь
            uploadObject = angular.extend({},
                url: url
                item: item
                deferred: uploadDeferred
                xhr: undefined
            )
            queue.push uploadObject
            #Start downloading the addition of the first file (until download is completed the rest of the items will be added to the same queue, or will create a new queue)
            #Начинаем загрузку при добавлении первого файла (пока загрузка не завершена остальные элементы будут добавляться в эту же очередь, иначе создастся новая очередь)
            if queue.length == 1
                queue.total = item[opts.fileSize]
                queue.loaded = 0
                queue.progress = 0
                queue.all = 1
                _uploadQueue()
            else
                queue.total += item[opts.fileSize]
                queue.all++
            uploadDeferred.promise

        _done   = ()->
            if typeof opts.done is 'function'
                opts.done 'Success!'

        #xhr-transport
        _xhrTransport = (uObj) ->
            xhr = new XMLHttpRequest
            form = new FormData
            xhr.item = uObj.item
            uObj.xhr = xhr
            form.append opts.fieldName, uObj.item._file, uObj.item.filename
            xhr.upload.addEventListener 'progress', ((e) ->
                scope.$apply ->
                    #Calculate the file upload progress
                    uObj.item[opts.fileLoaded] = if e.lengthComputable then e.loaded else undefined
                    uObj.item[opts.fileProgress] = if e.lengthComputable then Math.round(e.loaded * 100 / e.total) else undefined
                    scope.thumbs[queue.index].progress = uObj.item[opts.fileProgress] if scope.thumbs[queue.index]
                    #Calculate the overall progress of downloading all files
                    queue.loaded += uObj.item[opts.fileLoaded]
                    queue.progress = Math.round(queue.loaded * 100 / queue.total)
                    xhr.data = e
                    uObj.deferred.notify xhr
                    queue.index++
                    # Work with AngularJS 1.2. Not use for old versions
                    return
                return
            ), false
            xhr.addEventListener 'load', (->
                response = xhr.data = _parseJSON(xhr.responseText)
                #remove technical information about uploaded file from the model
                delete uObj.item._file
                delete uObj.item.$abort
                delete uObj.item[opts.fileProgress]
                delete uObj.item[opts.fileLoaded]
                delete uObj.item[opts.fileUploading]
                scope.$apply ->
                    if xhr.status == 200 and response
                        angular.extend uObj.item, response
                        uObj.deferred.resolve xhr
                    else
                        uObj.deferred.reject opts.setError('upload', xhr)
                    return
                return
            ), false
            xhr.addEventListener 'error', (->
                xhr.data = _parseJSON(xhr.responseText)
                scope.$apply ->
                    uObj.deferred.reject opts.setError('load', xhr)
                    return
                return
            ), false
            xhr.addEventListener 'abort', (->
                xhr.data = _parseJSON(xhr.responseText)
                scope.$apply ->
                    uObj.deferred.reject opts.setError('abort', xhr)
                    return
                return
            ), false
            xhr.open 'POST', uObj.url, true
            angular.forEach uObj.item._file.headers, (value, name) ->
                xhr.setRequestHeader name, value
                return
            xhr.send form
            return

        #iframe-transport
        _iframeTransport = (uObj) ->
            uObj.xhr = false
            form = uObj.item._file._form
            iframe = form.find('iframe')
            input = form.find('input')
            input.prop 'name', opts.fieldName
            form.prop
                action: uObj.url
                method: 'post'
                target: iframe.prop('name')
                enctype: 'multipart/form-data'
                encoding: 'multipart/form-data'
            iframe.bind 'load', ->
                response = undefined
                rawResponse = undefined
                # Wrap in a try/catch block to catch exceptions thrown
                # when trying to access cross-domain iframe contents:
                try
                    response = iframe.contents()
                    # Google Chrome and Firefox do not throw an
                    # exception when calling iframe.contents() on
                    # cross-domain requests, so we unify the response:
                    if !response.length or !response[0].firstChild
                        throw new Error
                    rawResponse = angular.element(response[0].body).text()
                    response = _parseJSON(rawResponse)
                catch e
                form.remove()
                #Remove hidden form
                delete uObj.item._file
                #remove technical information about uploaded file from the model
                scope.$apply ->
                    if response and !response.error
                        #It is impossible to know the status of loading into the frame, so we now if error when response contans error parameter
                        #Нельзя узнать статус загрузки во фрейм, поэтому ошибка определяется наличием параметра error в ответе
                        angular.extend uObj.item, response
                        uObj.deferred.resolve
                            item: uObj.item
                            response: response
                    else
                        uObj.deferred.reject opts.setError('upload',
                            responseText: rawResponse
                            data: response
                            item: uObj.item
                            dummy: true)
                    return
                return
            form[0].submit()
            return

        #Parse JSON
        _parseJSON = (data) ->
            if typeof data != 'object'
                try
                    return angular.fromJson(data)
                catch e
                    return false
            data

        #Cancel download
        _abort = ->
            #forEach is used for the closure for transmission each element value in setTimeout
            #forEach используется для организации замыкания для передачи каждого элемента value в setTimeout
            angular.forEach queue, ((value, key) ->
                if value.item == this
                    if value.xhr
                        #If you call abort without setTimeout, you get error: apply already in progress
                        #Если запустить abort напрямую возникает ошибка: apply already in progress
                        setTimeout (->
                            value.xhr.abort()
                            return
                        ), 0
                    else
                        #Remove the element from the queue that has not yet started to upload
                        #Удаляем из очереди элемент, который еще не начал загружаться
                        queue.splice key, 1
                    queue.total -= value.item[opts.fileSize]
                    queue.loaded -= value.item[opts.fileLoaded]
                    queue.all--
                    queue.index--
                return
            ), this
            return

        return

])

.directive('upThumb', ['upConfig','$q', (upConfig,$q)->
    link: (scope, elem, attrs) ->
        CropCanvas = undefined
        ResizeCanvas = undefined

        Crop = (image) ->
            CropCanvas = document.createElement('canvas')
            width = image.width
            height = image.height
            sx = 0; sy = 0;
            sw = width; sh = height;
            dx = 0; dy = 0;
            dw = sw; dh = sh;
            size = 160
            if width > height and width > size
                sw = dw = height
                sh = dh = height
                sx = (width - height) / 2
            else if height > width and height > size
                sw = dw = width
                sh = dh = width
                sy = (height - width) / 2
            CropCanvas.width = sw
            CropCanvas.height = sh
            ctx = CropCanvas.getContext('2d')
            ctx.drawImage image, sx, sy, sw, sh, dx, dy, dw, dh
            if ( CropCanvas.width > size )
                CropCanvas = Resize(CropCanvas, (Math.floor(CropCanvas.width / size) * size))
            while (CropCanvas.width > size)
                CropCanvas = Resize(CropCanvas, (CropCanvas.width - size))

            #CropCanvas = Resize(CropCanvas, size)
            elem.attr 'src', CropCanvas.toDataURL(image.mimeType)
            return

        Resize = (canvas, size) ->
            ResizeCanvas = document.createElement('canvas')
            ResizeCanvas.width = size
            ResizeCanvas.height = size
            ctx = ResizeCanvas.getContext('2d')
            ctx.drawImage canvas, 0, 0, ResizeCanvas.width, ResizeCanvas.height
            ResizeCanvas

        createImage = (src) ->
            defer = $q.defer()
            img = new Image

            img.onload = (data)->
                img.mimeType = /:(\w+\/\w+);/g.exec(data.target.src)[1]
                defer.resolve img
                return

            img.src = src
            defer.promise

        createImage(scope[attrs['upThumb']]['thumb']).then Crop, ->
            console.log 'error'
            return
        return
])
((angular, factory) ->
    if typeof define is "function" and define.amd
        define "angular-file-upload", [ "angular" ], (angular) ->
            factory angular

    else
        factory angular
) (if typeof angular is "undefined" then null else angular), (angular) ->
    'use strict'
    angular.module('FileUpload', [])
    .value(
        'FileUploadConfig'
        url         : '/'
        queue       : []
        previews    : []
        files       : []
        progress    : 0
        autoUpload  : false
    )
    .factory('FileUploader',[
        'FileUploadConfig'
        '$window'
        '$q'
        '$timeout'
        '$http'
        (FileUploadConfig,$window,$q,$timeout,$http)->
            console.log FileUploadConfig

            class FileUploader
                constructor : (options)->
                    settings = angular.copy(FileUploadConfig)
                    angular.extend(@, settings, options)
                    return @

                addToQueue : (files)->
                    ###@modifyFiles(files).then (data)->
                        console.log data###
                    promises = []
                    addToQueueDefer = $q.defer()
                    angular.forEach files, (file)->
                        promises.push(@readFile(file))
                        return
                    ,@
                    self = @
                    $q.all(promises).then ()->
                        addToQueueDefer.resolve(
                            self.previews.push.apply(self.previews,self.queue)
                        )
                    addToQueueDefer.promise

                readFile : (file)->
                    readFileDefer = $q.defer()
                    Reader = new FileReader()
                    Reader.onload = ((file)->
                        self = @
                        (e)->
                            result = e.target.result
                            Buffer = self.Uint8Array(result)
                            #Base64 = FileUploader.base64ArrayBuffer(result)
                            mimeType = self.mimeType(self.getFileHeader(Buffer))
                            if mimeType.ext isnt 'psd'
                                Base64 = self._arrayBufferToBase64(Buffer)
                                data = 'data:'+mimeType.type+';base64,'+Base64
                            else
                                parser = new PSD.Parser(Buffer)
                                parser.parse()
                                canvas = parser.imageData.createCanvas(parser.header)
                                data = canvas.toDataURL("image/png")

                            file = angular.extend {},{
                                lastModifiedDate: file.lastModifiedDate
                                lastModified: file.lastModified
                                size: file.size
                                type: file.type
                                name: file.name
                                full: data
                                thumb: data
                                file: file
                            },mimeType
                            self.queue.push(file)
                            readFileDefer.resolve(file)
                            #document.getElementById('uploadingImg').src = file.full
                            return
                    ).call(@,file)

                    Reader.readAsArrayBuffer(file);
                    readFileDefer.promise


                Uint8Array : (result)->
                    new Uint8Array(result) # Buffer

                _arrayBufferToBase64 : (Buffer) ->
                    binary = ''
                    len = Buffer.byteLength
                    i = 0
                    while i < len
                        binary += String.fromCharCode(Buffer[i])
                        i++
                    window.btoa binary

                getFileHeader : (Buffer)->
                    array = Buffer.subarray(0, 4)
                    header = ''
                    i = 0
                    while i < array.length
                        header += array[i].toString(16)
                        i++
                    return header

                mimeType : (headerString)->
                    console.log headerString
                    switch headerString
                        when "89504e47"
                            type = "image/png"
                            ext  = "png"
                        when "47494638"
                            type = "image/gif"
                            ext  = "gif"
                        when "ffd8ffe0", "ffd8ffe1", "ffd8ffe2", "ffd8ffdb"
                            type = "image/jpeg"
                            ext  = "jpeg"
                        when "25504446"
                            type = "application/pdf"
                            ext  = "pdf"
                        when "38425053"
                            type = "image/vnd.adobe.photoshop"
                            ext  = "psd"
                        when "63686b63"
                            type = "text/plain"
                            ext  = "txt"
                        else
                            type = "unknown"
                            ext  = "unknown"
                    return {
                        type : type
                        ext  : ext
                    }

                isHTML5 : !!($window['File'] && $window['FormData']);

                uploadFiles : ()->
                    ###self = @

                    if self.queue.length
                        console.log self.queue
                        self.xhrTransport().success (data)->
                            console.log data
                            angular.extend(self.queue[0],data)
                            self.files.push(self.queue[0])
                            self.queue.shift()
                            self.uploadFiles()
                            return###
                    self = @
                    uploadFilesDefer = $q.defer()

                    promises = @queue.map (queue)->
                        self.xhrTransport(queue.file).then (response)->
                            #angular.extend queue,response.data
                            self.files.push(response.data)
                            #uploadFilesDefer.resolve(response.data)
                            self.queue.shift()

                    $q.all(promises).then (result) ->
                        console.log result
                        uploadFilesDefer.resolve(self.files)
                        return

                    uploadFilesDefer.promise
                    #@xhrTransport()

                clear : ()->
                    @queue    = []
                    @previews = []
                    @files    = []
                    return

                xhrTransport : (file)->
                    file = file || @queue[0].file

                    FData = new FormData
                    FData.append 'Files', file

                    $http
                    .post @url, FData,
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}

                parseJSON   : (data) ->
                    if typeof data != 'object'
                        try
                            return angular.fromJson(data)
                        catch e
                            return false
                    data
    ])
    .directive('fuFile',[
        'FileUploader'
        (FileUploader)->
            restrict: 'EA'
            link    : (scope ,element, attr)->

                $uploader = scope.$eval(attr.uploader);

                if !($uploader instanceof FileUploader)
                    throw new TypeError('"Uploader" must be an instance of FileUploader')

                element.on 'change', ()->
                    #console.log @files
                    $uploader.addToQueue(@files).then ()->
                        console.log $uploader.previews
                        if typeof $uploader.add is 'function'
                            $uploader.add.call(null,$uploader.previews)

                        # Если включена автоматическая загрузка
                        # загружаем файлы сразу после добавления
                        if $uploader.autoUpload is true
                            $uploader.uploadFiles()
                            .then ()->
                                console.log $uploader.files
                                if typeof $uploader.done is 'function'
                                    $uploader.done.call(null,$uploader.files)

                        return

                    return

                ###uploader = scope.$eval(attr.uploader)

                console.log (uploader instanceof FileUploader)

                element.on 'change', ()->
                    #console.log @files
                    FileUploader.addToQueue(@files).then ()->
                        console.log FileUploader.queue
                    console.log FileUploader.queue[0]
                    return###
    ])
    .directive('fuThumb', ['$q', ($q)->
        restrict: 'EA'
        link    : (scope, elem, attrs) ->
            CropCanvas = undefined
            ResizeCanvas = undefined

            Crop = (image) ->
                CropCanvas = document.createElement('canvas')
                width = image.width
                height = image.height
                sx = 0; sy = 0;
                sw = width; sh = height;
                dx = 0; dy = 0;
                dw = sw; dh = sh;
                size = 160
                if width > height and width > size
                    sw = dw = height
                    sh = dh = height
                    sx = (width - height) / 2
                else if height > width and height > size
                    sw = dw = width
                    sh = dh = width
                    sy = (height - width) / 2
                CropCanvas.width = sw
                CropCanvas.height = sh
                ctx = CropCanvas.getContext('2d')
                ctx.drawImage image, sx, sy, sw, sh, dx, dy, dw, dh
                if ( CropCanvas.width > size )
                    CropCanvas = Resize(CropCanvas, (Math.floor(CropCanvas.width / size) * size))
                while (CropCanvas.width > size)
                    CropCanvas = Resize(CropCanvas, (CropCanvas.width - size))

                #CropCanvas = Resize(CropCanvas, size)
                thumbBase64 = CropCanvas.toDataURL(image.mimeType)
                scope[attrs['fuThumb']]['thumb'] = thumbBase64
                elem.attr 'src', thumbBase64
                return

            Resize = (canvas, size) ->
                ResizeCanvas = document.createElement('canvas')
                ResizeCanvas.width = size
                ResizeCanvas.height = size
                ctx = ResizeCanvas.getContext('2d')
                ctx.drawImage canvas, 0, 0, ResizeCanvas.width, ResizeCanvas.height
                ResizeCanvas

            createImage = (src) ->
                defer = $q.defer()
                img = new Image

                img.onload = (data)->
                    img.mimeType = /:(\w+\/\w+);/g.exec(data.target.src)[1]
                    defer.resolve img
                    return

                img.src = src
                defer.promise

            createImage(scope[attrs['fuThumb']]['thumb']).then Crop, ->
                console.log 'error'
                return
            return
    ])

    return
module.exports = ($timeout)->
    restrict: 'A'
    require: 'ngModel'
    priority: 99
    link: (scope, elm, attr, ngModelCtrl) ->
        if attr.type == 'radio' or attr.type == 'checkbox'
            return
        elm.unbind 'input'
        debounce = undefined
        elm.bind 'input', ->
            $timeout.cancel debounce
            debounce = $timeout((->
                scope.$apply ->
                    ngModelCtrl.$setViewValue elm.val()
                    return
                return
            ), attr.ngDebounce or 1000)
            return
        elm.bind 'blur', ->
            scope.$apply ->
                ngModelCtrl.$setViewValue elm.val()
                return
            return
        return
module.exports = ()->
    # A = attribute
    # E = Element
    # C = Class and
    # M = HTML Comment
    restrict:'A'
    link: (scope, element, attrs) ->
        element.sortable({
            connectWith: ".connectedSortable"
        }).disableSelection()
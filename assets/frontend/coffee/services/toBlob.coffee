if !HTMLCanvasElement::toBlob
    hasBlobConstructor = do ->
        try
            return Boolean(new Blob)
        catch ignore
            return false
        return
    hasArrayBufferViewSupport = hasBlobConstructor and do ->
            try
                return new Blob([ new Uint8Array(100) ]).size == 100
            catch ignore
                return false
            return

    HTMLCanvasElement::toBlob = (callback, opt_mime, opt_quality) ->
        __NEJS_THIS__ = this
        if typeof opt_mime == 'undefined'
            opt_mime = 'image/png'
        if typeof opt_quality == 'undefined'
            opt_quality = 1
        if opt_mime == 'image/png'
            callback @['msToBlob']()
            return
        byteString = atob(@toDataURL(opt_mime, opt_quality).replace(/[^,]*,/, ''))
        buffer = new ArrayBuffer(byteString.length)
        intArray = new Uint8Array(buffer)
        i = -1
        while ++i < byteString.length
            intArray[i] = byteString.charCodeAt(i)
        if hasBlobConstructor
            callback new Blob([ if hasArrayBufferViewSupport then intArray else buffer ], type: opt_mime)
            return
        builder = new (window['MSBlobBuilder'])
        builder['append'] buffer
        callback builder['getBlob'](opt_mime)
        return
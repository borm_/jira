'use strict'
global._ = require('lodash');
global.alertify = require '../js/plugins/alertify'
require 'angular'
require 'angular-bootstrap'
require 'ng-websocket'
#require 'restangular'
require 'ui-router'
require '../lib/bootstrap/dropdown'
require '../lib/bootstrap/collapse'
require 'ng-alertify'
require 'angular-ui-sortable'
require 'angular-local-storage'

#require './coffee/modules/Uploader'
require './coffee/modules/FileUpload'

require './coffee/modules/timeAgo'

#global.base64 = require '../js/plugins/base64'


app = angular.module 'BTS',[
    'ui.router'
    'ngWebsocket'
    'ui.bootstrap'
    'Alertify'
    'ui.sortable'
    'LocalStorageModule'
    'textAngular'
    #'Uploader'
    'FileUpload'
    'time.ago'
]
.config (
    $locationProvider
    $httpProvider
    $urlRouterProvider
    $websocketProvider
    $interpolateProvider
    $stateProvider
    localStorageServiceProvider
    $provide
) ->

    $provide.decorator 'taOptions', [
        'taRegisterTool'
        '$delegate'
        '$http'
        (taRegisterTool, taOptions, $http) ->
            ###taRegisterTool 'uploadImage',
                buttontext: 'Upload File'
                action: (deferred, restoreSelection) ->
                    editor = @$editor()
                    filepicker.pick { services: [ 'COMPUTER' ] }, ((blob) ->
                        $http.post('api/media', medium:
                            filename: blob.filename
                            url: blob.url
                            mimetype: blob.mimetype
                            size: blob.size).success (data, status, headers, config) ->
                        alert data.message
                        if new RegExp('image').test(blob.mimetype)
                            editor.wrapSelection 'insertImage', blob.url, true
                        else
                            element = document.createElement('a')
                            element.innerHTML = blob.filename
                            element.href = blob.url
                            editor.wrapSelection 'insertHTML', element.outerHTML, true
                        deferred.resolve()
                        restoreSelection()
                        return
                        return
                    ), (error) ->
                        # console.log(error.toString())
                        return
                    false###

            taOptions.toolbar = [
                [
                    #'h1'
                    #'h2'
                    #'h3'
                    #'h4'
                    #'h5'
                    #'h6'
                    'p'
                    'pre'
                    'quote'
                ]
                [
                    'bold'
                    'italics'
                    'underline'
                    'strikeThrough'
                    'ul'
                    'ol'
                ]
                [
                    'justifyLeft'
                    'justifyCenter'
                    'justifyRight'
                    'indent'
                    'outdent'
                ]
                [
                    #'uploadImage'
                    'insertImage'
                    'insertLink'
                    #'insertVideo'
                    #'wordcount'
                    #'charcount'
                ]
                [
                    'redo'
                    'undo'
                    'clear'
                ]
            ]
            taOptions
    ]

    $locationProvider.html5Mode({
        enabled: true
        requireBase: false
    })
    # $httpProvider
    $httpProvider.interceptors.push('httpInterceptor')

    $websocketProvider.$setup({
        lazy: false
        reconnect: true
        reconnectInterval: 2000
        mock: false
        enqueue: false
    })

    localStorageServiceProvider
    .setPrefix 'BTSStorage'

    # $interpolateProvider
    $interpolateProvider.startSymbol '[{'
    $interpolateProvider.endSymbol   '}]'

    # On the first page load disable ui-router so that
    # our server rendered page is not reloaded based on the window location.
    #$urlRouterProvider.deferIntercept()
    $urlRouterProvider.rule ($injector, $location)->
        re = /(.+)(\/+)(\?.*)?$/
        path = $location.url();

        if(re.test(path))
            return path.replace(re, '$1$3')

        return false

    $stateProvider
    # User
    .state 'login' , require './coffee/controllers/user/Login'
    .state 'logout', require './coffee/controllers/user/Logout'
    .state 'signup', require './coffee/controllers/user/SignUp'
    .state 'profile', require './coffee/controllers/user/Profile'
    # RapidBoard
    .state 'RapidBoard',
        require('./coffee/controllers/dashboard/List')['dashboard']
    .state "RapidBoard.task",
        angular.extend
            resolve     :
                getTask : ($timeout,$http, $stateParams)->
                    $http({method: 'GET',url: '/Task/get/'+$stateParams.id})
                preview : -> true
            require './coffee/controllers/dashboard/Task'
    .state 'task',
        angular.extend
            resolve     :
                getTask : ($http, $stateParams)->
                    $http({method: 'GET',url: '/Task/get/'+$stateParams.id})
                preview : -> false
            require './coffee/controllers/dashboard/Task'

.run ($rootScope, $state, $stateParams, $timeout)->

    $rootScope.$on "$stateChangeStart",
        (event, toState, toParams, fromState, fromParams, error) ->

    $rootScope.$on "$viewContentLoading", (event, viewConfig)->
        #$scope.firstLoading = false
        #console.log viewConfig

##
# Factories
#
app.factory 'ws', [
    '$websocket'
    require('./coffee/factories/ws')
]
app.factory 'httpInterceptor', [
    'Alertify'
    '$q'
    '$log'
    require './coffee/factories/httpInterceptor'
]

# For LocalStorage
app.factory 'localStorage', [
    '$http'
    '$q'
    'localStorageService'
    require './coffee/factories/localStorage'
]

##
# Directives
#
app.directive 'draggable', require('./coffee/directives/uiDraggable')
#app.directive 'sortable', require('./coffee/directives/uiSortable')
app.directive 'ngDebounce', [
    '$timeout'
    require('./coffee/directives/ngDebounce')
]
##
# Modules
#
app.controller 'BTSCtrl', [
    '$scope'
    '$http'
    'localStorageService'
    'localStorage'
    '$modal'
    '$log'
    '$rootScope'
    'FileUploader'
    require './coffee/controllers/BTSCtrl'
]
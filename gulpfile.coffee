gulp        = require 'gulp'
path        = require 'path'
watch       = require 'gulp-watch'
coffee      = require 'gulp-coffee'
less        = require 'gulp-less'
cssmin      = require 'gulp-cssmin'
gutil       = require 'gulp-util'
source      = require 'vinyl-source-stream'
concat      = require 'gulp-concat'
stringify   = require "stringify"
browserify  = require 'gulp-browserify'
uglify      = require 'gulp-uglify'

gulp.task 'coffee', ()->
    gulp.src ['./assets/frontend/frontend.coffee'], read: false
    .pipe browserify(
        transform : [
            'coffeeify', stringify extensions:['.hbs']
        ]
        extensions: ['.coffee']
    )
    .pipe concat 'frontend.js'
    .pipe gulp.dest './public/frontend/js'
    .pipe concat 'frontend.min.js'
    .pipe uglify mangle:false
    .pipe gulp.dest './public/frontend/js'
gulp.task 'watch_coffee', ()->
    gulp.watch([
        # coffees
        './assets/**/*.coffee'
        './assets/**/coffee/*.coffee'
        './assets/**/coffee/**/*.coffee'
        './assets/**/coffee/**/**/*.coffee'
        './assets/**/coffee/**/**/**/*.coffee'
        './assets/**/coffee/modules/**/hbs/*.hbs'
        # js
        './assets/**/js/*.js'
        './assets/**/js/**/*.js'
    ], ['coffee'])

# Compiles LESS > CSS
gulp.task 'less', ()->
    gulp.src ['./assets/frontend/frontend.less']
    .pipe less paths: [ path.join __dirname, 'less', 'includes' ]
    .pipe concat 'frontend.css'
    .pipe gulp.dest './public/frontend/css'
    .pipe concat 'frontend.min.css'
    .pipe cssmin()
    .pipe gulp.dest './public/frontend/css'
gulp.task 'watch_less', ()->
    gulp.watch([
        # less
        './assets/**/*.less'
        './assets/**/less/*.less'
        './assets/**/less/**/*.less'
        './assets/**/less/**/**/*.less'
    ], ['less'])

gulp.task 'dev', ()->
    revision = require './gulprevision'
    gulp.src [
        './static/frontend/js/frontend.js'
        './static/frontend/css/frontend.css'
    ], base: './static'
    ## Change file.patch and save to next line gulp.dest
    .pipe revision.revision('dev')
    .pipe gulp.dest './static'
    # Create json Config
    .pipe revision.jsonConf({path : './templates/config/dev/static.json'})
    .pipe gulp.dest './templates/config'

gulp.task 'prod', ()->
    revision = require './gulprevision'
    gulp.src [
        './static/frontend/js/frontend.min.js'
        './static/frontend/css/frontend.min.css'
    ], base: './static'
    ## Change file.patch and save to next line gulp.dest
    .pipe revision.revision('prod')
    .pipe gulp.dest './static/prod'
    # Create json Config
    .pipe revision.jsonConf({path : './templates/config/prod/static.json'})
    .pipe gulp.dest './templates/config'

gulp.task 'default', [
    'coffee'
    'watch_coffee'
    'less'
    'watch_less'
]
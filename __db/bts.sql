-- phpMyAdmin SQL Dump
-- version 4.4.3
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Май 12 2015 г., 21:30
-- Версия сервера: 5.6.24
-- Версия PHP: 5.4.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `bts`
--

-- --------------------------------------------------------

--
-- Структура таблицы `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `class` varchar(25) NOT NULL,
  `icon` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `groups`
--

INSERT INTO `groups` (`id`, `name`, `class`, `icon`) VALUES
(1, 'To Do', 'todo', 'fa-tasks'),
(2, 'In Progress', 'progress', 'fa-spinner fa-pulse'),
(3, 'Testing', 'testing', 'fa-question'),
(4, 'Done', 'done', 'fa-archive');

-- --------------------------------------------------------

--
-- Структура таблицы `priorities`
--

CREATE TABLE IF NOT EXISTS `priorities` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `class` varchar(25) NOT NULL,
  `icon` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `priorities`
--

INSERT INTO `priorities` (`id`, `name`, `class`, `icon`) VALUES
(1, 'Blocker', 'blocker', 'fa-ban'),
(2, 'Major', 'major', 'fa-long-arrow-up'),
(3, 'Minor', 'minor', 'fa-long-arrow-down');

-- --------------------------------------------------------

--
-- Структура таблицы `statuses`
--

CREATE TABLE IF NOT EXISTS `statuses` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `class` varchar(25) NOT NULL,
  `icon` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `statuses`
--

INSERT INTO `statuses` (`id`, `name`, `class`, `icon`) VALUES
(1, 'Resolved', 'resolve', 'fa-leaf'),
(2, 'Closed', 'close', 'fa-lock'),
(3, 'Reopened', 'reopen', 'fa-unlock-alt');

-- --------------------------------------------------------

--
-- Структура таблицы `tasks`
--

CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(11) NOT NULL,
  `author` int(11) NOT NULL,
  `assign` int(11) NOT NULL DEFAULT '0',
  `title` varchar(50) NOT NULL,
  `message` text NOT NULL,
  `description` text NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `priority` int(11) NOT NULL DEFAULT '0',
  `group` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  `time` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tasks`
--

INSERT INTO `tasks` (`id`, `author`, `assign`, `title`, `message`, `description`, `type`, `priority`, `group`, `status`, `time`) VALUES
(1, 1, 1, 'omelet without breaking eggs', 'One cannot make an omelet without breaking eggs', '', 1, 1, 2, 1, '1428608420885'),
(2, 1, 2, 'One cannot make', 'An omelet without breaking eggs', '', 1, 2, 1, 1, '1428608420885');

-- --------------------------------------------------------

--
-- Структура таблицы `types`
--

CREATE TABLE IF NOT EXISTS `types` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `class` varchar(25) NOT NULL,
  `icon` varchar(25) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `types`
--

INSERT INTO `types` (`id`, `name`, `class`, `icon`) VALUES
(1, 'Task', 'task', 'fa-tag'),
(2, 'Sub Task', 'sub-task', 'fa-tags'),
(3, 'New Feature', 'feature', 'fa-lightbulb-o'),
(4, 'Bug', 'bug', 'fa-bug');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `email` varchar(25) NOT NULL,
  `passwd` varchar(50) NOT NULL,
  `nick` varchar(25) DEFAULT NULL,
  `avatar` varchar(25) DEFAULT NULL,
  `role` int(11) DEFAULT NULL COMMENT 'userRole.id'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `email`, `passwd`, `nick`, `avatar`, `role`) VALUES
(1, 'efim4eg@gmail.com', '27463511', 'bORm', 'borm.png', 1),
(2, 'm.aliynik@gmail.com', '123', 'Mike', 'mike.jpg', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `userRole`
--

CREATE TABLE IF NOT EXISTS `userRole` (
  `id` int(11) NOT NULL,
  `role` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `userRole`
--

INSERT INTO `userRole` (`id`, `role`) VALUES
(1, 'developer'),
(2, 'tester');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `priorities`
--
ALTER TABLE `priorities`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `author` (`author`),
  ADD KEY `assign` (`assign`);

--
-- Индексы таблицы `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Индексы таблицы `userRole`
--
ALTER TABLE `userRole`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `priorities`
--
ALTER TABLE `priorities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `types`
--
ALTER TABLE `types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

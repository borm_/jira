Mysql = require 'app/core/mysql'
orm = new Mysql
LocalStrategy   = require('passport-local').Strategy;

module.exports = new LocalStrategy(
        usernameField : 'email',
        passwordField : 'passwd',
        passReqToCallback : true
    ,
    (req, email, password, done)->
        options = {
            query: 'select * from `user` u where u.email = ":email"'
            criteria:
                email   : email
                passwd  : password
        }
        orm.query options.query, options.criteria, (err, rows, fields)->
            if rows && rows[0]
                user = rows[0]

            if !user
                done(null, false,
                    status : 'error'
                    message: 'User not found!'
                )
            else
                if password isnt user.passwd
                    done(null, false,
                        status : 'error'
                        message: 'Incorrect password'
                    )
                else
                    done(null, rows[0],
                        status : 'success'
                        message: 'authorization successful'
                    )
)
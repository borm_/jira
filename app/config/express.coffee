express = require 'express'
session = require 'express-session'

cookieParser = require 'cookie-parser'
bodyParser = require 'body-parser'

MongoStore = require('connect-mongo')(session)

path = require 'path'

module.exports = (app,passport)->

    ##
    # Middlewares
    ##
    app.use(cookieParser()); # read cookies (needed for auth)
    app.use(bodyParser.json()); # get information from html forms
    app.use(bodyParser.urlencoded({extended: true}));

    ##
    # Static
    ##
    app.use('/media', express.static('uploads'));
    app.use('/static', express.static('public'));
    app.use('/assets', express.static('assets'));
    app.use('/views', express.static('views'));
    app.use (req, res, next) ->
        res.locals.static = '/static'
        res.locals.media = '/media'
        res.locals.avatars = '/media/avatars/'
        next()

    ##
    # Engine
    ##
    app.set('views', path.join('views'));
    expHbs = require('express-handlebars')
    hbs = expHbs.create({
        extname: 'hbs'
        layoutsDir : 'views/layouts'
        defaultLayout: 'base.hbs'
        partialsDir: 'views/partials'
        helpers:
            ng      : (data) ->
                return '[['+data+']]';
    })
    app.engine 'hbs', hbs.engine
    app.set('view engine', 'hbs');

    app.use(session({
        secret: config.mongo.secret_key,
        #maxAge: new Date(Date.now() + 3600000),
        store: new MongoStore({
            url: config.mongo.db.url
        }),
        resave: true,
        saveUninitialized: true
    }));

    # Passport:
    app.use(passport.initialize());
    app.use(passport.session());

    # Multer
    # Configure the multer.
    #global.done = false
    ###app.use(multer(
        dest    : './uploads/'
        rename  : (fieldname, filename)->
            return filename+Date.now()
        onFileUploadStart: (file)->
            console.log(file.originalname + ' is starting ...',global.done)
        onFileUploadComplete: (file)->
            console.log(file.fieldname + ' uploaded to  ' + file.path)
            global.done=true
    ))###
module.exports = (app)->
    # catch 500 and forward to error handler
    app.use (err, req, res, next) ->
        res.status(err.status || 500);

        if req.xhr
            return res.json
                error:
                    name: err.name
                    message: err.message
                    errors: err.errors

        res.render 'error',
            message: err.message,
            error: {}
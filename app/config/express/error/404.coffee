module.exports = (app)->
    # catch 404 and forward to error handler
    app.use (req, res, next) ->
        err = new Error('Not Found');
        err.status = 404;
        next(err);
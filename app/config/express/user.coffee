module.exports = (app)->
    app.use (req,res,next) ->
        if req.user
            #console.log req.user
            res.locals.user         = req.user;
            res.locals.user.status  = true;
        else
            res.locals.user = {
                name    : 'Гость'
                status  : false
                image   : 'noavatar.png'
            };
        next();
express = require 'express'
app = express()
env = app.get 'env'

console.log env

# Init core
# core = require('./core/tiny')

global.config = require('./config/env')[env]
global.config.root = root
global._ = require 'lodash'

## Multer
multer  = require 'multer'
# Configure the Multer
global.done = false
global.config.multer =
    rename  : (fieldname, filename)->
        filename = filename+Date.now()
        return md5 filename
    onFileUploadStart: (file)->
        console.log(file.originalname + ' is starting ...',global.done)
    onFileUploadComplete: (file)->
        console.log(file.fieldname + ' uploaded to  ' + file.path)
        global.done=true

crypto = require 'crypto'
md5 = (str)->
    crypto.createHash('md5').update(str).digest('hex')

#console.log config

# Passport
passport = require 'passport'
require('./config/passport')(passport)

# Express
require('./config/express')(app,passport)

# Middleware
require('./config/express/user')(app)

require('./modules')(app,multer)

app.get '/modules/:Module/:Name', (req,res)->
    # После NODE_PATH=. нужно убирать первый "/"
    res.render req.path.substr(1), layout: 'clear'

require('./config/express/error/404')(app)
require('./config/express/error/500')(app)

server = app.listen 3173, ->
    host = server.address().address
    port = server.address().port

    console.log 'Example app listening at http://%s:%s', host, port

module.exports = app;
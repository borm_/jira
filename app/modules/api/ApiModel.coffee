Mysql = require 'app/core/mysql/mysql'
orm = new Mysql

module.exports =

    ###
        LocalStorage
    ###
    Users : (req,res)->
        orm.query
            'query'  : 'select
                        u.id,u.email,u.name,u.avatar,r.role
                        from `user` u
                        inner join `userRole` r
                        on u.role = r.id'
            'params' : []
            'done'   : (err, rows) ->
                if rows.length isnt 0
                    result =
                        status      : 'success'
                        users       : rows
                res.send result

    Types : (req,res)->
        orm.query
            'query'  : 'select * from `types`'
            'params' : []
            'done'   : (err, rows) ->
                if rows.length isnt 0
                    result =
                        status      : 'success'
                        types       : rows
                res.send result

    Groups : (req,res)->
        orm.query
            'query'  : 'select * from `groups`'
            'params' : []
            'done'   : (err, rows) ->
                if rows.length isnt 0
                    result =
                        status      : 'success'
                        groups      : rows
                res.send result

    Statuses : (req,res)->
        orm.query
            'query'  : 'select * from `statuses`'
            'params' : []
            'done'   : (err, rows) ->
                if rows.length isnt 0
                    result =
                        status      : 'success'
                        statuses    : rows
                res.send result

    Priorities : (req,res)->
        orm.query
            'query'  : 'select * from `priorities`'
            'params' : []
            'done'   : (err, rows) ->
                if rows.length isnt 0
                    result =
                        status      : 'success'
                        priorities  : rows
                res.send result

    ###
        Tasks
    ###
    Tasks : (req,res)->
        orm.query
            'query'  : 'select * from `tasks`'
            'params' : []
            'done'   : (err, rows) ->
                if rows.length isnt 0
                    result =
                        status : 'success'
                        tasks  : rows
                res.send result
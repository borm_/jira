Model = require './ApiModel'

module.exports =
    ###
        LocalStorage
    ###
    users       : (req, res)->
        Model.Users(req, res)

    types       : (req, res)->
        Model.Types(req, res)

    groups      : (req, res)->
        Model.Groups(req, res)

    statuses    : (req, res)->
        Model.Statuses(req, res)

    priorities  : (req, res)->
        Model.Priorities(req, res)

    ###
        Tasks
    ###
    tasks : (req, res)->
        Model.Tasks(req, res)

    ###
        Upload
    ###
    upload :
        image : (req,res)->
            if global.done is true
                global.done = false
                Files = req.files['Files']
                console.log Files
                res.send(
                    status      : 'success'
                    message     : "File uploaded."
                    original    : Files.originalname
                    name        : Files.name
                    mimetype    : Files.mimetype
                    extension   : Files.extension
                    size        : Files.size
                )


Mysql = require 'app/core/mysql/mysql'
orm = new Mysql
crypto = require('crypto')

module.exports = (app,multer)->

    app.get '/api/users'        , require('./ApiController')['users']
    app.get '/api/types'        , require('./ApiController')['types']
    app.get '/api/groups'       , require('./ApiController')['groups']
    app.get '/api/statuses'     , require('./ApiController')['statuses']
    app.get '/api/priorities'   , require('./ApiController')['priorities']

    app.get '/api/tasks'        , require('./ApiController')['tasks']

    # File Upload
    app.post '/api/upload/image/',[
        multer(_.assign(config.multer, dest : './uploads/tasks/')),
        require('./ApiController')['upload']['image']
    ]

    app.post '/api/upload/avatar/',[
        multer(_.assign(config.multer, dest : './uploads/avatars/')),
        require('./ApiController')['upload']['image']
    ]

    app.get '/download/:id', (req,res)->
        if parseInt(req.params.id)
            orm.query
                'query'  : 'select * from attachments where id=:id'
                'params' :
                    id : parseInt(req.params.id)
                'done'   : (err, rows) ->
                    if rows.length isnt 0
                        file = rows[0]
                        link = 'uploads/tasks/'+file.name
                        res.download(link,file.original)
        else
            res.send 'File not Found!'

    app.get '/crypto', (req,res)->
        res.send md5('АУДИ 1.psd'+Date.now())

    md5 = (str)->
        crypto.createHash('md5').update(str).digest('hex')

    app.get '/attach', (req,res)->

        values = [
            [null,24,'180.jpg','1801434365948779.jpg','image/jpeg','jpg','25556']
            [null,24,'180.jpg','1801434365948779.jpg','image/jpeg','jpg','25556']
        ]

        orm.query
            'query'  : 'select * from tasks where id=:id; select * from attachments where tid=:id'
            'params' :
                id : 58
            'done'   : (err, rows) ->
                if rows.length isnt 0
                    task = _.assign rows[0][0], {
                        attachments : rows[1]
                    }
                    result =
                        status : 'success'
                        task   : task
                res.send result
        ,
            multipleStatements: true

module.exports = (app,multer)->
    require('./api/ApiRoute')(app,multer)
    require('./user/userRoute')(app)
    require('./main/MainRoute')(app)
    require('./dashboard/DashboardRoute')(app)
    require('./task/TaskRoute')(app)
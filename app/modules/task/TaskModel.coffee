Mysql = require 'app/core/mysql/mysql'
orm = new Mysql
easyimg = require('easyimage')
PSD = require('psd')

module.exports =

    Create  : (req,res)->
        orm.query
            'query'  : 'insert into `tasks` (
                author,assign,
                title,message,description,
                type,priority,`group`,status
            ) values (
                ":author",":assign",
                ":title",":message",":description",
                ":type",":priority",":group",":status"
            )'
            'params' :
                author      : req.user.id || req.body.author
                assign      : req.body.assign
                title       : req.body.title
                message     : req.body.message
                description : req.body.description
                type        : req.body.type
                priority    : req.body.priority
                group       : req.body.group
                status      : req.body.status
            'done'   : (err, result) ->
                if !result.insertId
                    result =
                        status  : 'error'
                        message : 'Create task error'
                else
                    result =
                        status  : 'success'
                        message : 'Task created'
                        id      : result.insertId
                res.send result

    Attach  :
        Add     : (req,res)->
            tid = req.body.id
            values = _.map req.body.attachments, (file)->

                file.thumb = file.name

                if file.extension is 'psd'
                    file.thumb = file.name+'.png'

                    pngImage = './uploads/tasks/thumbs/'+file.name+'.png'
                    # You can also use promises syntax for opening and parsing
                    PSD.open('./uploads/tasks/'+file.name).then((psd) ->
                        psd.image.saveAsPng pngImage
                    ).then ()->
                        console.log 'Finished!'
                        easyimg.rescrop(
                            src : pngImage
                            dst : pngImage
                            width:160, height:160,
                            cropwidth:160, cropheight:160,
                            x:0, y:0
                            fill:true
                            quality:100
                        ).then (image)->
                            console.log "Resized and cropped: " + image.width + " x " + image.height
                            #console.log image
                        , (err)->
                            console.log err
                        return

                else
                    easyimg.rescrop(
                        src : './uploads/tasks/'+file.name
                        dst : './uploads/tasks/thumbs/'+file.name
                        width:160, height:160,
                        cropwidth:160, cropheight:160,
                        x:0, y:0
                        fill:true
                    ).then (image)->
                        console.log "Resized and cropped: " + image.width + " x " + image.height
                        #console.log image
                    , (err)->
                        console.log err

                [null,tid,file.original,file.name,file.thumb,file.mimetype,file.extension,file.size]

            orm.query
                'query'  : 'insert into `attachments` values ?'
                'params' : [values]
                'done'   : (err, rows) ->
                    if rows.length isnt 0
                        result =
                            status : 'success'
                            task   : rows
                    res.send result

    Comment :
        Add     : (req,res)->
            orm.query
                'query'  : 'insert into `comments` (aid,tid,text) values (:aid,:tid,":text")'
                'params' :
                    aid : req.user.id || req.body.aid
                    tid : req.body.tid
                    text: req.body.text
                'done'   : (err, rows) ->
                    if rows.length isnt 0
                        result =
                            status : 'success'
                            task   : rows
                    res.send result

    Get     : (req,res)->
        orm.query
            'query'  : 'select * from tasks where id=:id;
                        select * from attachments where tid=:id;
                        select * from comments where tid=:id'
            'params' :
                id : req.params.id
            'done'   : (err, rows) ->
                if rows.length isnt 0
                    task = _.assign rows[0][0], {
                        attachments : rows[1]
                        comments    : rows[2]
                    }
                    result =
                        status : 'success'
                        task   : task
                res.send result
        ,
            multipleStatements: true

    Change  :

        Group   : (req,res)->
            orm.query
                'query'  : 'update `tasks` set `group` = :gid where id = :tid'
                'params' : {
                    gid : req.body.gid
                    tid : req.body.tid
                }
                'done'   : (err, result) ->
                    if result.affectedRows is 0
                        result =
                            status  : 'error'
                            message : 'Task id not found'
                    else
                        result =
                            status  : 'success'
                            message : 'Task group changed'
                    res.send result

        Assign  : (req,res)->
            orm.query
                'query'  : 'update `tasks` set `assign` = :aid where id = :tid'
                'params' : {
                    aid : req.body.aid
                    tid : req.body.tid
                }
                'done'   : (err, result) ->
                    if result.changedRows is 0
                        result =
                            status  : 'error'
                            message : 'Task id not found'
                    else
                        result =
                            status  : 'success'
                            message : 'Task Assign changed'
                    res.send result
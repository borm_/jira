module.exports = (app)->

    app.post '/Task/create',
        require('./TaskController')['Create']

    app.post '/Task/attach/Add',
        require('./TaskController')['Attach']['Add']

    app.post '/Task/comment/Add',
        require('./TaskController')['Comment']['Add']

    app.get '/Task/get/:id',
        require('./TaskController')['Get']

    app.post '/Task/change/Group',
        require('./TaskController')['Change']['Group']

    app.post '/Task/change/Assign',
        require('./TaskController')['Change']['Assign']

    app.post '/Task/Resolve',
        require('./TaskController')['Resolve']
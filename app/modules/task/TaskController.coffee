Model = require './TaskModel'

module.exports =

    Create  : (req,res)->
        Model.Create(req,res)

    Attach  :
        Add     : (req,res)->
            Model.Attach.Add(req,res)

    Comment :
        Add     : (req,res)->
            Model.Comment.Add(req,res)

    Get     : (req, res)->
        Model.Get(req, res)

    Change  :
        Group   : (req,res)->
            Model.Change.Group(req,res)

        Assign  : (req,res)->
            Model.Change.Assign(req,res)

    Resolve : (req,res)->
        Model.Resolve(req,res)
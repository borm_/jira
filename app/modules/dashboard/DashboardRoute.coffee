module.exports = (app)->

    app.get '/dashboard',
        require('./DashboardController')['dashboard']

    app.get '/task/:id',
        require('./DashboardController')['task']
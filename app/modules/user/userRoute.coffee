module.exports = (app)->

    # Login
    app.get '/login', require('./login/LoginController')['get']
    app.post '/login', require('./login/LoginController')['post']

    # Logout
    app.post '/logout', require('./logout/LogoutController')['post']

    # Sign Up
    app.get '/signup', require('./signup/SignUpController')['get']
    app.post '/signup', require('./signup/SignUpController')['post']

    app.get '/profile/Get', require('./profile/ProfileController')['Get']
    app.post '/profile/Save', require('./profile/ProfileController')['Save']
    app.post '/profile/Avatar', require('./profile/ProfileController')['Avatar']
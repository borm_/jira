Model = require './ProfileModel'

module.exports =

    Get : (req,res)->
        Model.Get(req,res)

    Save: (req,res)->
        Model.Save(req,res)

    Avatar: (req,res)->
        Model.Avatar(req,res)

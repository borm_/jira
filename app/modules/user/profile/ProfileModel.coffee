Mysql = require 'app/core/mysql/mysql'
orm = new Mysql
easyimg = require('easyimage')

module.exports =

    Get : (req,res)->
        console.log req.user
        orm.query
            'query'  : 'select u.id,u.email,u.passwd,u.name,u.avatar,r.role from user u
                        inner join userRole r on u.role = r.id
                        where u.id=:id'
            'params' :
                id : req.user.id
            'done'   : (err, rows) ->
                if req.xhr || req.headers.accept.indexOf('json') > -1 && rows.length isnt 0
                    result =
                        status : 'success'
                        user   : rows[0]
                else
                    result =
                        status : 'error'
                        user   : null
                res.send result

    Avatar : (req,res)->

        file = req.body.avatar

        easyimg.rescrop(
            src : './uploads/avatars/'+file.name
            dst : './uploads/avatars/'+file.name
            width:200, height:200,
            cropwidth:200, cropheight:200,
            x:0, y:0
            fill:true
        ).then (image)->
            console.log "Resized and cropped: " + image.width + " x " + image.height
        , (err)->
            console.log err

        orm.query
            'query'  : 'update `user` set `avatar`=":avatar" where `id`=:id'
            'params' :
                avatar  : file.name
                id      : req.user.id
            'done'   : (err, rows) ->
                if rows.length isnt 0
                    result =
                        status : 'success'
                        avatar : file.name
                res.send result

    Save: (req,res)->
        orm.query
            'query'  : 'update `user`
                        set `name`=":name",`email`=":email",`passwd`=":passwd"
                        where `id`=:id'
            'params' :
                name    : req.body.name
                email   : req.body.email
                passwd  : req.body.passwd
                id      : req.user.id
            'done'   : (err, rows) ->

                console.log err

                if rows.length isnt 0
                    result =
                        status : 'success'
                        user   : rows
                res.send result
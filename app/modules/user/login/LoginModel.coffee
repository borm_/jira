passport = require 'passport'
Mysql = require 'app/core/mysql'
orm = new Mysql

module.exports =

    Login : (req,res,next)->

        if req.body.email && req.body.passwd
            passport.authenticate('local', (err, user, result) ->
                if err
                    return next(err)
                if user
                    req.logIn user, (err) ->
                        next err if err
                res.send result
                return
            ) req, res, next
        else
            res.send(
                status : 'error'
                message: 'Заполните все поля!!!'
            )
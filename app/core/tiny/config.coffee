module.exports =
    dir_components: 'components'
    # The used components
    components:
        CMySqlDb:
            host: 'localhost',
            user: 'root',
            password: '',
            database: 'pn.ua',
    # Data base
    db:
        component: 'CMySqlDb'
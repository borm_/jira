###*
#
###
class CModel extends include 'base/CBase'
    constructor: ->
        super

    ###*
    # The table name in Db
    # @return string: The table name in db
    ###
CModel::table_name = ->
    throw new (@core.Exception().CNotImplementedError) 'The function "table_name" must be implemented!'

###*
# Show columns in table
###
CModel::show_columns = (callback, connection=null) ->
    new (@core.SQLCommand())(connection).show_columns().from(@table_name()).query(callback)

###*
#
###
CModel::select = (expr, connection=null) ->
    c = new (@core.SQLCommand())(connection)
    c.select(expr)

###*
# Find records by condition
###
CModel::find = (callback, condition=null, values={}, connection=null) ->
    new (@core.SQLCommand())(connection).select().from(@table_name()).where(condition).query(callback, {}, values)

module.exports = CModel
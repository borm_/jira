class SQLConditionCommand extends include 'base/CBase'
    constructor: (@_condition) ->
        super

SQLConditionCommand::toString = ->
    @_condition.map(
        (i) ->
            # Create condition
            cc = (operator) ->
                Object.keys(i[1]).map((k) -> k + operator + i[1][k])

            switch i[0]
                when '=' then cc '='
                when '!=' then cc '!='
                when '<' then cc '<'
                when '>' then cc '>'
                when '<=' then cc '<='
                when '>=' then cc '>='
    ).join(' AND ')

# SQLConditionCommand::valueOf = -> 555

class SQLCommand extends include 'base/CBase'
    ###*
    # @param object|null _connection: Connection to db
    ###
    constructor: (@_connection) ->
        super
        @_component = @core.db_component()
        @_explain = ''
        @_select_exp = ''
        @_from = ''
        @_where_condition = ''

###*
# @param string sql: SQL query
# @param object params: Parameters for SQL query
# @param object values: Values for SQL query
###
SQLCommand::_format_sql = (sql, params={}, values={}) ->
    This = @
    return sql if not params? or not values?

    sql.replace /\:(\w+)/g, (v, k) ->
        if params.hasOwnProperty(k)
            return This._component.dbms.escapeId(params[k])
        else if values.hasOwnProperty(k)
            return This._component.dbms.escape(values[k])

###*
# Execute sql as a string
# @param string sql: Raw SQL
# @param function callback: Result function
# @param object params: Parameters for SQL query
# @param object values: Values for SQL query
###
SQLCommand::raw_query = (sql, callback, params={}, values={}) ->
    This = @
    @_connection ?= @_component.create_connection()

    # Format raw SQL query
    prepared_sql = @_format_sql(sql, params, values)
    console.log(sql, '->', prepared_sql)

    @_connection.query(prepared_sql, (e, rows) ->
        if e
            callback.call(This, e)
        else
            callback.call(This, rows)

        This._connection.destroy()
    )

SQLCommand::show_columns = (@table_name) ->
    @_select_exp = 'SHOW COLUMNS'
    @

SQLCommand::explain = ->
    @_explain = 'EXPLAIN '
    @

SQLCommand::select = (@expr=null) ->
    # Expression
    @_select_exp = 'SELECT ' + if @expr instanceof Array then @expr.map((i) -> ':' + i).join(',') else if @expr then @expr else '*'
    @

SQLCommand::from = (@table_name) ->
    @_from = ' FROM ' + if @table_name instanceof Array then (':tbn_' + i + ' t' + i for i, v of @table_name).join(',') else ':tbn'
    @

SQLCommand::where = (condition=null) ->
    @_where_condition = (' WHERE ' + new SQLConditionCommand(condition)) if condition?
    @

SQLCommand::query = (callback, params={}, values={}) ->
    # Merge params
    params[v] = v for k, v of @expr if @expr instanceof Array

    if @table_name instanceof Array
        for i, v of @table_name
            params['tbn_' + i] = v
    else
        params['tbn'] = @table_name

    @raw_query(@_explain + @_select_exp + @_from + @_where_condition, callback, params, values)

module.exports = SQLCommand
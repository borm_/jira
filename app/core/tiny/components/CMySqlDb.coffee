class CMySqlDb extends include 'base/CComponent'
    constructor: (_name, _config) ->
        super(_name, _config)

        @dbms = require 'mysql'

###*
# Create connection
###
CMySqlDb::create_connection = ->
    @dbms.createConnection(@_config);

module.exports = CMySqlDb
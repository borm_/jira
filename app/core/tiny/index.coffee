# Including core files
global.include = (file) ->
    require __dirname + '/' + file

class Init
    constructor: (@_config) ->
        @path = require 'path'

        # Init base classes
        @_base = @base()

        # Init components classes
        @_component_list = @components()

Init::base = ->
    # The base classes list
    list =
        # The exceptions list
        Exception: include 'base/Exception'
        #
        SQLCommand: include 'base/SQLCommand'
    return list

Init::components = ->
    # The components list
    list = {}
    console.log('Init -> components')

    # Check on dir components
    return 0 if not @_config.dir_components?

    for name, config of @_config.components
        # If class component is not set
        if not config.class
            # Init default component, Params: (name, config)
            list[name] = new (include @path.join(@_config.dir_components, name)) name, config
        else
            # Init user component, Params: (name, config)
            list[name] = new (include @path.join(@_config.dir_components, config.class)) name, config

    return list

###*
# Singleton
###
class Core
    constructor: (@_config) ->
        return if global.core?
        # Ref to current instance
        global.core = @
        # Init classes
        @_init = new Init(@_config)

###*
# The list initialized components or component by name
# @param string name: The name component
# @return object:
###
Core::components = (name=null) -> if name? then @_init._component_list[name] else @_init._component_list

###*
# The base classes list or base class by name
# @param string name: The name base class
###
Core::base_classes = (name=null) -> if name? then @_init._base[name] else @_init._base

###*
# Db configuration
# @param string key: The configuration key
###
Core::db = (key=null) -> if key? then @_config.db[key] else @_config.db

###*
# @return object: Return instance Db component
###
Core::db_component = -> @components(@db('component'))

###*
# Returns exception by name or exceptions list
# @param string name: The name exception
# @return object: Exception
###
Core::Exception = (name=null) -> if name? then @base_classes('Exception')[name] else @base_classes('Exception')

###*
#
###
Core::SQLCommand = -> @base_classes('SQLCommand')

#
new Core(require './config')

# Tests
# require './tests'
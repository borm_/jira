mysql = require('mysql')

class MySql

    constructor: () ->

    query : (query,params,callback)->
        @connection = mysql.createConnection(config.mysql.db)
        @connection.connect()
        @connection.config.queryFormat = (query,values) ->
            if (!values)
                return query;
            query.replace /\:(\w+)/g, (txt, key) ->
                if (values.hasOwnProperty(key))
                    return this.escape(values[key]);
                return txt

        params = params || []
        @_connection = @connection
        @_connection.query query, params, (err, rows, fields) ->
            if err
                throw (err);
            callback(err, rows, fields)
            @_connection.end()
        #connection.connect()

module.exports = MySql
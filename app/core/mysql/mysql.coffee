mysql = require('mysql')

sanitize = require('sanitize-html')

class MySql

    constructor: () ->

    query : (o,@config)->

        @config = @config || {}
        @config = _.assign config.mysql.db, @config

        @connection = mysql.createConnection(@config)
        @connection.connect()

        if _.isPlainObject o.params

            @connection.config.queryFormat = (query,values) ->
                if (!values)
                    return query;
                query.replace /\:(\w+)/g, (txt, key) ->
                    if (values.hasOwnProperty(key))
                        #return this.escape(values[key]).replace(/%20/g,' ');
                        return sanitize(values[key])
                    return txt

        o.params = o.params || []
        @_connection = @connection
        @_connection.query o.query, o.params
        , (err, rows, fields) ->
            if err
                throw (err);
            o.done(err, rows, fields)
            @_connection.end()

module.exports = MySql